import functions

class findcommonterms:

    def __init__(self):
        # We will keep terms from all summaries in 'common_terms' dictionary
        self.common_terms = {}
        self.final_terms = []

    def step(self, *args):
        # First, split summary to list of terms
        terms = str(args[0]).split()

        for current in terms:
            # if dictionary does not have this term, then add this
            # term in dictionary and set num of times shown as 1
            if self.common_terms.get(current) is None:
                self.common_terms[current] = 1
            # if this term has shown before in dictionary,
            # we just increase num of times shown
            else:
                self.common_terms[current] += 1
        pass

    def final(self):

        # Once we have all terms from all summaries they gave us
        # We sort terms by number of times shown
        terms_sorted = sorted(self.common_terms.items(), key=lambda x: x[1], reverse=True)

        # we keep the first 10% of sorted list (with unique items)
        ten_percent = len(terms_sorted) / 10.0
        ten_percent += 0.5
        for i in range(0, int(ten_percent)):
            self.final_terms += [terms_sorted[i][0]]

        # Begin the return procedure
        yield ('10%_top_common_terms',)
        for i in self.final_terms:
            yield [i]


findcommonterms.registered = True
