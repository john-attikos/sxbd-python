import functions

class madissum:

    def __init__(self):
        self.sum = 0

    def step(self, *args):
        print "--> ", args[0]

    def final(self):
        return self.sum

madissum.registered = True
