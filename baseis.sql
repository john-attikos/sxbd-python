eksigisi apo lepto 37 http://delos.uoa.gr/opendelos/player?rid=44269b27

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
1o erotima
	strsplitv tou madis (uparxei kai sta paradeigmata)
	Stin ousia pairnoume sto summary, tis protaseis, to spame to ena kato apto allo 
	kanoume data join , kai na bgaloume ta baroi, gia na doume poies katigories einai
	oi pio pithanes gia to kathe keimeno


def classify(pubid, topn):

    # Check for valid user input from web page
    if (pubid == "Unknown input") or (topn == "Unknown input"):
        return [("Please ", "fill ", "the ", " boxes!", "<img src='kidding.png'>")]

    # Create a new connection
    con = connection()

    # Create a cursor on the connection
    cur = con.cursor()

    # first declare variables in database to prevent SQLInjection
    cur.execute("select var('articles_id',?)", (pubid,))
    cur.execute("select var('topn' ,?)", (topn,))

    results = cur.execute("SELECT t1.titlos, t2.klasi, t2.ypoklasi, SUM(t2.baros) "
                          + "FROM ( "
                          + "    SELECT strsplitv(summary) as sum1, a1.title as titlos "
                          + "    FROM articles a1 "
                          + "    WHERE a1.id = var('articles_id') "
                          + "    ) as t1, "
                          + "    ( "
                          + "    SELECT c.term as oros, c.weight as  baros, c.class as klasi, c.subclass as ypoklasi "
                          + "    FROM classes c "
                          + "    ) as t2 "
                          + "WHERE t1.sum1 = t2.oros "
                          + "GROUP BY t2.klasi, t2.ypoklasi "
                          + "ORDER BY SUM(t2.baros) DESC "
                          + "LIMIT var('topn') ")



    returnlist = [("No", "Class", "Subclass", "Weight's sum", "Article's title")]
    results_counter = 0
    for row in final_result:
        results_counter += 1
        if results_counter > int(topn):
            break
        returnlist += [(results_counter, row[0][0], row[0][1], row[1], title)]

    return returnlist



SELECT 	t2.klasi, t2.ypoklasi, SUM(t2.baros)

FROM 	(
 	SELECT 	strsplitv(summary) as sum1, a1.title as titlos
 	FROM 	articles a1
 	WHERE 	a1.id=365
 	) as t1,
 	(
 	SELECT 	c.term as oros, c.weight as  baros, c.class as klasi, c.subclass as ypoklasi
 	FROM 	classes c
 	) as t2


WHERE 	t1.sum1 = t2.oros
GROUP BY t2.klasi, t2.ypoklasi
ORDER BY SUM(t2.baros) DESC
LIMIT 100
;




@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
2i sunartisi (38:40)
	Idio, alla i ilopoiisi tha ginei aptin pleura tis python, o strsplitv de tha xrisimopoithei.
	Osa queries theloume tha ginei me sql stin python... (de xreiazetai var, mporoume me ton 
	tropo tis sql)



@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
3i sunartisi (update...)
	Apla ena update... opou tropopoioume ta baroi



@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
4i
	De xreiazetai oute edo kati apo madis, (mporoume me apli sql...)



	SELECT 	auth.id, count(*)

	FROM 	authors auth,
	 	articles art,
	 	authors_has_articles aa,
	 	article_has_class ac

	WHERE 	art.id = ac.article_id
	 	AND art.id = aa.articles_id
	 	AND aa.authors_id = auth.id
	 	AND ac.class = 'Mathematics'

	GROUP BY auth.id
	ORDER BY count(*) DESC
	LIMIT 10
	;



@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
5i
	O sunathroistikos telestis findCommonTerms, tha ton upoloiisoume emeis sto madis, 
	Kai tha pairnei oles tis perilipseis sto input tou, isos to spaei
	se lekseis, tha briskei tis pio suxnes lekseis... kai sto telos tha epistrefei mia 
	sxesi pou se kathe ploiada tha periexei mia apo autes tis suxnes lekseis, oi lekseis autes
	de tha prepei na summetexoun ston upologismo tis Jaccard...
	DEN tha kanoume update sti basi diagrafontas tis lekseis....
	UPDATE tha kanoume mono sto 3o


	UPDATE articles
	SET summary='1 2 3 4 5 6 7 8 9 10 11'
	WHERE id = 555
	;

	INSERT INTO articles
	(id, title, summary)
	VALUES
	(555, '555title leme', '1 2 3');


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////kalo  (OXI PIA)
	SELECT 	tt1.firstID, t2.id2, jaccard( tt1.grouped1, jgroup(t2.split2))

	FROM 	(
	 	SELECT 	jgroup(t1.split1) as grouped1, t1.id1 as firstID
	 	FROM 	(
 	 	 	SELECT a1.id as id1, strsplitv(a1.summary) as split1 
 	 	 	FROM articles a1 
 	 	 	WHERE a1.id=365
 	 	 	) as t1
	 	) as tt1,
 	 	(
 	 	SELECT 	a2.id as id2, strsplitv(a2.summary) as split2 
 	 	FROM 	articles a2 
 	 	WHERE 	a2.id != 365
 	 	) as t2

 	GROUP BY t2.id2
 	ORDER BY jaccard( tt1.grouped1, jgroup(t2.split2)) DESC
 	LIMIT 10
	;
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	SELECT 	t1.split1
	FROM 	(
	 	SELECT 	strsplitv(a1.summary) as split1, a1.id as id1
	 	FROM 	articles a1 
	 	WHERE 	a1.id = 41
	 	) as t1
 	
	EXCEPT

	SELECT 	findcommonterms(summary)
	FROM 	articles
	;
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////// MAGIKO LEME...........
	/////////////// MAGIKO LEME...........

	SELECT 	tt1.firstID, t2.id2, jaccard( jexcept( tt1.grouped1, commontermsTable.groupedcommon ), jexcept( jgroup(t2.split2), commontermsTable.groupedcommon))

	FROM 	(
	 	SELECT 	jgroup(t1.split1) as grouped1, t1.id1 as firstID
	 	FROM 	(
 	 	 	SELECT a1.id as id1, strsplitv(a1.summary) as split1 
 	 	 	FROM articles a1 
 	 	 	WHERE a1.id=365
 	 	 	) as t1
	 	) as tt1,
 	 	(
	 	SELECT 	jgroup(commonterms.common) as groupedcommon
	 	FROM 	(
 	 	 	SELECT 	findcommonterms(summary) as common
 	 	 	FROM 	articles
 	 	 	) as commonterms
	 	) as commontermsTable,
 	 	(
 	 	SELECT 	a2.id as id2, strsplitv(a2.summary) as split2 
 	 	FROM 	articles a2 
 	 	) as t2

 	GROUP BY t2.id2
 	ORDER BY jaccard( jexcept( tt1.grouped1, commontermsTable.groupedcommon ), jexcept( jgroup(t2.split2), commontermsTable.groupedcommon)) DESC
 	LIMIT 20
	;
	/////////////// MAGIKO LEME...........TELOS
	/////////////// MAGIKO LEME...........TELOS




	/////////////dokimi xoris jexcept
	/////////////dokimi xoris jexcept
	/////////////dokimi xoris jexcept
	/////////////dokimi xoris jexcept
	/////////////dokimi xoris jexcept
	/////////////dokimi xoris jexcept
	/////////////dokimi xoris jexcept
	/////////////dokimi xoris jexcept
	/////////////dokimi xoris jexcept
	/////////////dokimi xoris jexcept SUCCEDED!!!!!!!!!!!!



SELECT 	t1.id1, t2.id2, jaccard(t1.grouped1, t2.grouped2) as similiraty
FROM 	(
 	SELECT 	t11.id11 as id1, jgroup(t11.split11) as grouped1

 	FROM 	(
 	 	SELECT 	id as id11, strsplitv(summary) as split11
 	 	FROM 	articles 
 	 	WHERE 	id = 377
 	 	) as t11

 	WHERE 	t11.split11 not in (SELECT findcommonterms(summary) FROM articles)
 	GROUP BY id1
 	) as t1,
 	(
 	SELECT 	t22.id22 as id2, jgroup(t22.split22) as grouped2
 	FROM 	(
 	 	SELECT id as id22, strsplitv(summary) as split22 
 	 	FROM articles 
 	 	) as t22
 	WHERE 	t22.split22 not in (SELECT findcommonterms(summary) FROM articles)
 	GROUP BY id2
 	) as t2
GROUP BY t2.id2
ORDER BY similiraty DESC
LIMIT 20
;




365|365|1.0
365|17|0.078431372549
365|120|0.0634920634921
365|201|0.0609756097561
365|306|0.0547945205479
365|67|0.0535714285714
365|308|0.0519480519481
365|211|0.0508474576271
365|339|0.0487804878049
365|8|0.046875
365|111|0.0454545454545
365|283|0.0434782608696
365|324|0.0422535211268
365|130|0.0416666666667
365|265|0.0416666666667
365|209|0.04
365|239|0.0394736842105
365|207|0.0392156862745
365|62|0.0384615384615
365|256|0.0384615384615









