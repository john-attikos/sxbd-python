# ----- CONFIGURE YOUR EDITOR TO USE 4 SPACES PER TAB ----- #
import settings
import sqlite3
import sys


def connection():
    ''' User this function to create your connections '''
    import sys
    sys.path.append(settings.MADIS_PATH)
    import madis

    con = madis.functions.Connection('articles.db')

    return con


def classify(pubid, topn):

    # Check for valid user input from web page
    if (pubid == "Unknown input") or (topn == "Unknown input"):
        return [("Please ", "fill ", "the ", " boxes!", "<img src='kidding.png'>")]

    # Create a new connection
    con = connection()

    # Create a cursor on the connection
    cur = con.cursor()

    # first declare variables in database to prevent SQLInjection
    cur.execute("select var('articles_id',?)", (pubid,))
    cur.execute("select var('topn' ,?)", (topn,))

    # First lets verify if this article even exists
    exists = False
    verify = cur.execute("select id from articles where id = var('articles_id')")
    for i in verify:
        exists = True
    if not exists:
        return [("", "This article's id does not exists!")]

    # The whole thing in one query
    results = cur.execute("SELECT t2.klasi, t2.ypoklasi, SUM(t2.baros), t1.titlos "
                          + "FROM ( "
                          + "    SELECT strsplitv(summary) as sum1, a1.title as titlos "
                          + "    FROM articles a1 "
                          + "    WHERE a1.id = var('articles_id') "
                          + "    ) as t1, "
                          + "    ( "
                          + "    SELECT c.term as oros, c.weight as  baros, c.class as klasi, c.subclass as ypoklasi "
                          + "    FROM classes c "
                          + "    ) as t2 "
                          + "WHERE t1.sum1 = t2.oros "
                          + "GROUP BY t2.klasi, t2.ypoklasi "
                          + "ORDER BY SUM(t2.baros) DESC "
                          + "LIMIT var('topn') ")

    # Convert query results to list
    my_list = list(results.fetchall())

    # Begin return procedure
    returnlist = [("No", "Class", "Subclass", "Weight's sum", "Article's title")]
    results_counter = 0
    for row in my_list:
        results_counter += 1
        if results_counter > int(topn):
            break
        returnlist += [(results_counter, row[0], row[1], row[2], row[3])]

    return returnlist


def classify_plain_sql(pubid, topn):

    # Check for valid user input from web page
    if (pubid == "Unknown input") or (topn == "Unknown input"):
        return [("Please ", "fill ", "the ", " boxes!", "<img src='kidding.png'>")]

    # Create a new connection
    # WITHOUT USING ANYTHING that has to do with madIS
    con = sqlite3.connect('articles.db')

    # Create a cursor on the connection
    cur = con.cursor()

    # lets find articles title
    query_title = cur.execute("SELECT title "
                              + "FROM articles "
                              + "WHERE id = ?", [pubid])

    # lets verify if this article even exists
    exists = False
    for i in query_title:
        exists = True
    if not exists:
        return [("", "This article's id does not exists!")]


    title = ""
    for i in query_title:
        title = i[0]

    # Get this articles summary
    summary = cur.execute("SELECT summary "
                          + "FROM articles "
                          + "WHERE id = ? ", [pubid])

    # How we'll implement the summary split:

    # 1) Make a temporary list with a single item (the summary)
    my_list = []
    for i in summary:
        my_list = i[0]  # all words in a list

    # 2) Convert this single item to string
    string = ''.join(my_list)

    # 3) Make our words list by splitting the string
    words = string.split()

    # get all classes/subclasses
    classes = cur.execute("SELECT class,subclass "
                          + "FROM classes "
                          + "GROUP BY class,subclass ")

    # we'll make a dictionary with keys:(class,subclass) and values: weight
    class_weights = {}
    for i in classes:
        class_weights[i] = 0  # initialize all weights with zero

    # Now, for every WORD of articles summary,
    # get class/subclass/weight that has this WORD
    cur2 = con.cursor()
    for current_term in words:
        results = cur2.execute("SELECT class,subclass,weight "
                               + "FROM classes "
                               + "WHERE term = ? ", [current_term])
        # and update the dictionary with the weights of class/subclass
        for i in results:
            class_weights[(i[0], i[1])] += i[2]

    # make a tuple with sorted (by weight) items in descending order
    final_result = sorted(class_weights.items(), key=lambda x: x[1], reverse=True)

    # Begin return procedure
    returnlist = [("No", "Class", "Subclass", "Weight's sum", "Article's title")]
    results_counter = 0
    for row in final_result:
        results_counter += 1
        if results_counter > int(topn):
            break
        returnlist += [(results_counter, row[0][0], row[0][1], row[1], title)]

    return returnlist


def updateweight(class1, subclass, term, weight):

    # Check for valid user input from web page
    if (class1 == "Unknown input") or (subclass == "Unknown input") or (term == "Unknown input") or (weight == "Unknown input"):
        return [("Please ", "fill ", "the ", " boxes!", "<img src='kidding.png'>")]

    # Create a new connection
    con = connection()
    
    # Create a cursor on the connection
    cur = con.cursor()

    # First, check if this class/subclass/term exists
    cur.execute("select var('given_class',?) ", (class1,))
    cur.execute("select var('given_subclass' ,?) ", (subclass,))
    cur.execute("select var('given_term' ,?) ", (term,))
    result = cur.execute("SELECT weight "
                         + "FROM classes "
                         + "WHERE class = var('given_class') "
                         + "AND subclass = var('given_subclass') "
                         + "AND term = var('given_term') ")

    my_list = list(result.fetchall())
    # if result is empty
    if not my_list:
        return [("Error: ", "This Class or Subclass or Term does not exist...")]

    old_weight = my_list[0]  # find average weight
    new_weight = (old_weight[0] + float(weight)) / 2

    print old_weight
    print new_weight

    # Update database with new weight
    cur.execute("select var('new_weight',?) ", (new_weight,))
    cur.execute("UPDATE classes "
                + "SET weight = var('new_weight') "
                + "WHERE class = var('given_class') "
                + "AND subclass = var('given_subclass') "
                + "AND term = var('given_term') ")

    return [("OK ", "!")]


def selectTopNauthors(class1, n):

    # Check for valid user input from web page
    if (class1 == "Unknown input") or (n == "Unknown input"):
        return [("Please ", "fill ", "the ", " boxes!", "<img src='kidding.png'>")]

    # Create a new connection
    con = connection()
    
    # Create a cursor on the connection
    cur = con.cursor()

    # SQLInjection protection
    cur.execute("select var('n' ,?)", (n,))
    cur.execute("select var('class1' ,?)", (class1,))

    # This query will give us everything we want...
    results = cur.execute("SELECT auth.id, count(*) "
                          + "FROM authors auth, "
                            "    articles art, "
                            "    authors_has_articles aa, "
                            "    article_has_class ac "
                          + "WHERE art.id = ac.article_id "
                          + "    AND art.id = aa.articles_id "
                          + "    AND aa.authors_id = auth.id "
                          + "    AND ac.class = var('class1') "
                          + "GROUP BY auth.id "
                          + "ORDER BY count(*) DESC "
                          + "LIMIT var('n') ")

    # Convert query results to list
    my_list = list(results.fetchall())

    # Begin return procedure
    returnlist = [("No", "Author's ID", "Number of relevant publications")]
    results_counter = 0
    for row in my_list:
        results_counter += 1
        print row[0], row[1]
        if results_counter > int(n):
            break
        returnlist += [(results_counter, row[0], row[1])]

    return returnlist


# Declare external connection variable to be initialised only once!
global_con = connection()
# If this connection is made every time we run 5th query (findSimilarArticles()),
# Exception:
# KeyError('ITER\x1e<generator object final at 0x7fab1b4773c0>',)
# will happen... and it is not our business to fix that :p

def findSimilarArticles( articleId, n):

    # Check for valid user input from web page
    if (articleId == "Unknown input") or (n == "Unknown input"):
        return [("Please ", "fill ", "the ", " boxes!", "<img src='kidding.png'>")]

    # Create a new connection
    # con = connection()
    con = global_con

    # Create a cursor on the connection
    cur = con.cursor()

    cur.execute("select var('articleId',?)", (articleId,))
    cur.execute("select var('topNarticles',?)", (n,))

    # First lets verify if this article even exists
    exists = False
    verify = cur.execute("select id from articles where id = var('articleId')")
    for i in verify:
        exists = True
    if not exists:
        return [("", "This article's id does not exists!")]

    results = cur.execute("SELECT t1.id1, t2.id2, jaccard(t1.grouped1, t2.grouped2) as similiraty "
                          + "FROM ( "
                          + "    SELECT t11.id11 as id1, jgroup(t11.split11) as grouped1 "
                          + "    FROM ( "
                          + "        SELECT id as id11, strsplitv(summary) as split11 "
                          + "        FROM articles "
                          + "        WHERE id = var('articleId') "
                          + "    ) as t11 "
                          + "    WHERE t11.split11 not in (SELECT findcommonterms(summary) FROM articles) "
                          + "    GROUP BY id1 "
                          + "    ) as t1, "
                          + "    ( "
                          + "    SELECT t22.id22 as id2, jgroup(t22.split22) as grouped2 "
                          + "    FROM ( "
                          + "        SELECT id as id22, strsplitv(summary) as split22 "
                          + "        FROM articles "
                          + "        WHERE id != var('articleId') "
                          + "    ) as t22 "
                          + "    WHERE t22.split22 not in (SELECT findcommonterms(summary) FROM articles) "
                          + "    GROUP BY id2 "
                          + "    ) as t2 "
                          + "GROUP BY t2.id2 "
                          + "ORDER BY similiraty DESC "
                          + "LIMIT var('topNarticles') ")

    # Convert query results to list
    my_list = list(results.fetchall())

    # Begin return procedure
    returnlist = [("No", "Article ID", "Similarity")]
    results_counter = 0
    for row in my_list:
        results_counter += 1
        if results_counter > int(n):
            break
        returnlist += [(results_counter, row[1], row[2])]

    return returnlist
